## Descripción 
La aplicación ha sido desarrollada tratando de seguir las buenas prácticas de codificación más comunes, he puesto principal atención a la arquitectura tratando de cumplir los principios SOLID y utilizando siempre inyección de dependencias para mantener el código lo más desacoplado, testeable e independiente posible. En relación a los test unitarios, como norma solo testeo los métodos con lógica de negocios, no trato de buscar el 100 % de code coverage, en este caso solo unos pocos métodos tenían lógica de negocios. La App está dividida en 4 capas Dominio, Presentación, DataAccess y Services. Dado el poco tiempo me concentre en la arquitectura base más que en el diseño del sistema. 

### Mejoras 
- Verificar posibles condiciones de carrera.
- Paginado.
- Es muy lento eliminar toda la data y luego almacenarla de nuevo en la Base de Datos, en un entorno real podría utilizarse Redis y algún sistema de colas o Scheduling para procesar la persistencia de la data. 
- Entre otras...

## Tecnologías usadas 

### Backend 
- .NET Core 2.1 
- Entity Framework Core 
- SQL Server (Para backup y logs) 
- Kestrel 
- Microsoft Unit Test Framework 
- Moq 

### Frontend 
- Node.js 
- Angular 6 / RxJS 
- Angular Material 
- Bootstrap 
- Font Awesome 

### Herramientas necesarias para correr esta cosa
- Visual Studio 2017 con .NET Core 2.1
- SQL Server 
- Node.js npm y ng-cli 

Primero desde Visual Studio restaurar los paquetes Nuget y luego ejecutar la aplicación para tener el backend, desde appsettings.json se puede configurar la cadena de conexión, también se puede forzar a que la aplicación utilice la data de backup con solo cambiar TransactionsServiceUrl a cualquier otra cosa para que cause un error. 

Una vez el backend este corriendo, ir a la carpeta frontend/GloiathBank y desde una línea de comandos en ese directorio ejecutar npm install, luego 'ng serve' (suponiendo que se tiene instalado ng-cli).