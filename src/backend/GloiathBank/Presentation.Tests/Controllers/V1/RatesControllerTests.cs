using Domain;
using Domain.Logging;
using Domain.Repositories.Read;
using Domain.Repositories.Write;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Presentation.Controllers.V1;
using Presentation.ViewModels;
using Services.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Presentation.Tests.Controllers.V1
{
    [TestClass]
    public class RatesControllerTests
    {
        // SUT
        private RatesController _ratesController;
        private ICurrencyRateService _currencyRateService;
        private Mock<ICurrencyRateReadRepository> _mockReadRepository;
        private Mock<ICurrencyRateWriteRepository> _mockWriteRepository;
        private Mock<ICurrencyRateReadRepository> _mockBackupReadRepository;
        private Mock<ILogger> _mockLogger;

        private readonly List<CurrencyRate> _defaultCurrencyRate = new List<CurrencyRate>
        {
            new CurrencyRate { From = "USD", To = "CAD", Rate = 1.22M }
        };

        private readonly List<CurrencyRate> _backupCurrencyRate = new List<CurrencyRate>
        {
            new CurrencyRate { From = "USD", To = "CAD", Rate = 1.02M }
        };

        [TestInitialize]
        public void SetUp()
        {
            _mockReadRepository = new Mock<ICurrencyRateReadRepository>();
            _mockWriteRepository = new Mock<ICurrencyRateWriteRepository>();
            _mockBackupReadRepository = new Mock<ICurrencyRateReadRepository>();
            _mockLogger = new Mock<ILogger>();
            _mockReadRepository.Setup(m => m.GetAll()).Returns(() => _defaultCurrencyRate.AsQueryable());
            _mockWriteRepository.Setup(m => m.SaveAll(It.IsAny<IEnumerable<CurrencyRate>>()));
            _mockBackupReadRepository.Setup(m => m.GetAll()).Returns(() => _backupCurrencyRate.AsQueryable());
            _mockLogger.Setup(m => m.LogInformation(It.IsAny<string>()));
            _mockLogger.Setup(m => m.LogWarning(It.IsAny<string>()));
            _mockLogger.Setup(m => m.LogError(It.IsAny<string>()));
        }

        [TestMethod]
        public void ShouldReturnOk()
        {
            _currencyRateService = new CurrencyRateService(_mockReadRepository.Object, _mockWriteRepository.Object, _mockBackupReadRepository.Object, _mockLogger.Object);
            _ratesController = new RatesController(_currencyRateService);
            var result = _ratesController.Get() as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual((int)HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void ShouldReturnDataFromBackup()
        {
            _mockReadRepository.Setup(m => m.GetAll()).Returns(() => null);
            _currencyRateService = new CurrencyRateService(_mockReadRepository.Object, _mockWriteRepository.Object, _mockBackupReadRepository.Object, _mockLogger.Object);
            _ratesController = new RatesController(_currencyRateService);
            var result = _ratesController.Get() as OkObjectResult;
            Assert.IsNotNull(result);
            var model = result.Value as RateViewModel;
            Assert.AreEqual(true, model.FromBackup);
        }
    }
}
