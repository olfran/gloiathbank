using Domain;
using Domain.Logging;
using Domain.Repositories.Read;
using Domain.Repositories.Write;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Presentation.Controllers.V1;
using Presentation.ViewModels;
using Services.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Presentation.Tests.Controllers.V1
{
    [TestClass]
    public class TransactionsControllerTests
    {
        // SUT
        private TransactionsController _transactionController;
        // SUT
        private ITransactionService _transactionService;
        private Mock<ITransactionReadRepository> _mockReadRepository;
        private Mock<ITransactionWriteRepository> _mockWriteRepository;
        private Mock<ITransactionReadRepository> _mockBackupReadRepository;
        private Mock<ILogger> _mockLogger;

        private readonly List<CurrencyTransaction> _defaultTransaction = new List<CurrencyTransaction>
        {
            new CurrencyTransaction { Amount = 1, Currency = "USD", SKU = "A" }
        };

        private readonly List<CurrencyTransaction> _backupTransaction = new List<CurrencyTransaction>
        {
            new CurrencyTransaction { Amount = 2, Currency = "EUR", SKU = "B" }
        };

        [TestInitialize]
        public void SetUp()
        {
            _mockReadRepository = new Mock<ITransactionReadRepository>();
            _mockWriteRepository = new Mock<ITransactionWriteRepository>();
            _mockBackupReadRepository = new Mock<ITransactionReadRepository>();
            _mockLogger = new Mock<ILogger>();
            _mockReadRepository.Setup(m => m.GetAll()).Returns(() => _defaultTransaction.AsQueryable());
            _mockWriteRepository.Setup(m => m.SaveAll(It.IsAny<IEnumerable<CurrencyTransaction>>()));
            _mockBackupReadRepository.Setup(m => m.GetAll()).Returns(() => _backupTransaction.AsQueryable());
            _mockLogger.Setup(m => m.LogInformation(It.IsAny<string>()));
            _mockLogger.Setup(m => m.LogWarning(It.IsAny<string>()));
            _mockLogger.Setup(m => m.LogError(It.IsAny<string>()));
        }

        [TestMethod]
        public void ShouldReturnOk()
        {
            _transactionService = new TransactionService(_mockReadRepository.Object, _mockWriteRepository.Object, _mockBackupReadRepository.Object, _mockLogger.Object);
            _transactionController = new TransactionsController(_transactionService);
            var result = _transactionController.Get("A") as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual((int)HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void ShouldReturnBadRequest()
        {
            _transactionService = new TransactionService(_mockReadRepository.Object, _mockWriteRepository.Object, _mockBackupReadRepository.Object, _mockLogger.Object);
            _transactionController = new TransactionsController(_transactionService);
            var result = _transactionController.Get(null) as BadRequestResult;
            Assert.IsNotNull(result);
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode);
        }

        [TestMethod]
        public void ShouldReturnDataFromBackup()
        {
            _mockReadRepository.Setup(m => m.GetAll()).Returns(() => null);
            _transactionService = new TransactionService(_mockReadRepository.Object, _mockWriteRepository.Object, _mockBackupReadRepository.Object, _mockLogger.Object);
            _transactionController = new TransactionsController(_transactionService);
            var result = _transactionController.Get("A") as OkObjectResult;
            Assert.IsNotNull(result);
            var model = result.Value as TransactionViewModel;
            Assert.AreEqual(true, model.FromBackup);
        }
    }
}
