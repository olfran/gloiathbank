﻿using Domain;
using System.Collections.Generic;

namespace Presentation.ViewModels
{
    public class RateViewModel
    {
        public IEnumerable<CurrencyRate> Rates { get; set; }
        public bool FromBackup { get; set; }
    }
}
