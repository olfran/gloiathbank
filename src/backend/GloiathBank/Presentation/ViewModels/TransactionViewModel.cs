﻿using Domain;
using System.Collections.Generic;

namespace Presentation.ViewModels
{
    public class TransactionViewModel
    {
        public IEnumerable<CurrencyTransaction> Transactions { get; set; }
        public bool FromBackup { get; set; }
    }
}
