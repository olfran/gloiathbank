﻿using Microsoft.AspNetCore.Mvc;
using Presentation.ViewModels;
using Services.Data;

namespace Presentation.Controllers.V1
{
    [Route("api/v1/[controller]")]
    public class RatesController : Controller
    {
        private readonly ICurrencyRateService _currencyRateService;

        public RatesController(ICurrencyRateService currencyRateService)
        {
            _currencyRateService = currencyRateService;
        }

        // GET api/v1/rates
        [HttpGet]
        public IActionResult Get()
        {
            var rates = _currencyRateService.GetAll(null, out bool fromBackup);
            RateViewModel viewModel = new RateViewModel
            {
                Rates = rates,
                FromBackup = fromBackup
            };
            return Ok(viewModel);
        }
    }
}
