﻿using System.Collections.Generic;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Presentation.ViewModels;
using Services.Data;

namespace Presentation.Controllers.V1
{
    [Route("api/v1/[controller]")]
    public class TransactionsController : Controller
    {
        private readonly ITransactionService _transactionService;

        public TransactionsController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        // GET api/v1/transactions?sku=123
        [HttpGet]
        public IActionResult Get(string sku)
        {
            IEnumerable<CurrencyTransaction> result;
            if (string.IsNullOrEmpty(sku))
            {
                return BadRequest();
            }
            result = _transactionService.GetAll(filter => filter.SKU.StartsWith(sku.ToUpper()), out bool fromBackup);
            var viewModel = new TransactionViewModel
            {
                Transactions = result,
                FromBackup = fromBackup
            };
            return Ok(viewModel);
        }
    }
}
