﻿using DataAccess.Logging;
using DataAccess.Repositories.Read;
using DataAccess.Source.ORM;
using DataAccess.Source.Read;
using DataAccess.Source.Write;
using Domain;
using Domain.Logging;
using Domain.Repositories.Read;
using Domain.Repositories.Write;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Services.Data;

namespace Presentation.Init
{
    /// <summary>
    /// Setup the application's dependencies
    /// </summary>
    public static class DependencyInjectionSetup
    {
        public static void AddBankDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            string ratesServiceUrl = configuration["RatesServiceUrl"];
            string transactionsServiceUrl = configuration["TransactionsServiceUrl"];
            string connectionString = configuration["ConnectionString"];
            GloiathContext dbContext = new GloiathContext(connectionString);

            ////////////////////////////
            // Repositores
            ////////////////////////////
            ITransactionReadRepository transactionWebServiceReadRepository =
                new TransactionReadRepository(
                    new WebServiceReadContext<CurrencyTransaction>(transactionsServiceUrl));

            ITransactionWriteRepository transactionWriteRepository =
                new TransactionWriteRepository(
                    new DatabaseWriteContext<CurrencyTransaction>(dbContext));

            ITransactionReadRepository transactionBackupReadRepository =
                new TransactionReadRepository(
                    new DatabaseReadContext<CurrencyTransaction>(dbContext));

            ICurrencyRateReadRepository currencyRateWebServiceReadRepository =
                new CurrencyRateReadRepository(
                    new WebServiceReadContext<CurrencyRate>(ratesServiceUrl));

            ICurrencyRateWriteRepository currencyRateWriteRepository =
                new CurrencyRateWriteRepository(
                    new DatabaseWriteContext<CurrencyRate>(dbContext));

            ICurrencyRateReadRepository currencyRateBackupReadRepository =
                new CurrencyRateReadRepository(
                    new DatabaseReadContext<CurrencyRate>(dbContext));

            ////////////////////////////
            // Logging
            ////////////////////////////
            ILogger logger = new DatabaseLogger(dbContext);

            ////////////////////////////
            // Services
            ////////////////////////////
            services.AddSingleton<ICurrencyRateService>(_ =>
            {
                return new CurrencyRateService(currencyRateWebServiceReadRepository,
                    currencyRateWriteRepository, currencyRateBackupReadRepository, logger);
            });
            services.AddSingleton<ITransactionService>(_ =>
            {
                return new TransactionService(transactionWebServiceReadRepository,
                    transactionWriteRepository, transactionBackupReadRepository, logger);
            });
        }
    }
}
