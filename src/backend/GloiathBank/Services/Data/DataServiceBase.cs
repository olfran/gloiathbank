﻿using Domain;
using Domain.Exceptions;
using Domain.Logging;
using Domain.Repositories.Read;
using Domain.Repositories.Write;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Services.Data
{
    /// <summary>
    /// Base service to retrieve the information from the data source
    /// and save the information retrieved in backup
    /// </summary>
    /// <typeparam name="T">DomainBase</typeparam>
    public class DataServiceBase<T> : IDataServiceBase<T> where T : DomainBase
    {
        protected readonly IReadRepository<T> _readRepository;
        protected readonly IWriteRepository<T> _writeRepository;
        protected readonly IReadRepository<T> _backupReadRepository;
        protected readonly ILogger _logger;

        /// <summary>
        /// For this constructor we need 3 dependencies
        /// </summary>
        /// <param name="readRepository">Default read repository</param>
        /// <param name="writeRepository">Default write repository</param>
        /// <param name="backupReadRepository">Backup repository (In case we can't read from the Default repository)</param>
        /// <param name="logger">Logger</param>
        public DataServiceBase(
            IReadRepository<T> readRepository,
            IWriteRepository<T> writeRepository,
            IReadRepository<T> backupReadRepository,
            ILogger logger)
        {
            _readRepository = readRepository;
            _writeRepository = writeRepository;
            _backupReadRepository = backupReadRepository;
            _logger = logger;
        }

        /// <summary>
        /// Retrieves all the data from the source
        /// </summary>
        /// <param name="where">Where predicate</param>
        /// <param name="fromBackup">Indicates if the data comes from backup store</param>
        /// <returns>List of T</returns>
        /// <exception cref="Domain.Exceptions.NoDataAvailableException">No Data</exception>
        public IEnumerable<T> GetAll(Expression<Func<T, bool>> where, out bool fromBackup)
        {
            try
            {
                return GetAllAux(where, out fromBackup);
            }
            catch (Exception ex)
            {
                _logger.LogError($"No data available neither from the default data source nor the Database, we're doomed | {ex}");
                throw;
            }
        }

        private IEnumerable<T> GetAllAux(Expression<Func<T, bool>> where, out bool fromBackup)
        {
            IQueryable<T> result = null;
            fromBackup = false;
            try
            {
                result = _readRepository.GetAll();
                if (result == null) { throw new NoDataAvailableException(); }
            }
            catch (Exception)
            {
                fromBackup = true;
                _logger.LogWarning("I Can't get data from the default origin, I'll try with the backup");
                // We couldn't get the data from the default data source, so we use the backup repository
                result = _backupReadRepository.GetAll();
                // No data available neither from the default data source nor the backup, we're doomed
                if (result == null) { throw; }
                _logger.LogInformation("Data was retrieved from the Backup because I couldn't connect to the default data origin");
            }
            // Everything went ok from the default repo, so we save the new data in backup
            if (!fromBackup)
            {
                // TODO: Handle race condition
                {
                    _writeRepository.RemoveAll(typeof(T));
                    _writeRepository.SaveAll(result);
                }
            }
            if (where != null) { result = result.Where(where); }
            _logger.LogInformation("Information retrieved successfully");
            return result.ToList();
        }
    }
}
