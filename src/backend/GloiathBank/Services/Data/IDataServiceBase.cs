﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Domain;

namespace Services.Data
{
    public interface IDataServiceBase<T> where T : DomainBase
    {
        IEnumerable<T> GetAll(Expression<Func<T, bool>> where, out bool fromBackup);
    }
}