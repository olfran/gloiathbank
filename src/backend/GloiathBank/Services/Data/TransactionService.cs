﻿using Domain;
using Domain.Logging;
using Domain.Repositories.Read;
using Domain.Repositories.Write;

namespace Services.Data
{
    /// <summary>
    /// Retrieves all the Transactions information from the data source
    /// </summary>
    public class TransactionService : DataServiceBase<CurrencyTransaction>, ITransactionService
    {
        public TransactionService(
            ITransactionReadRepository readRepository,
            ITransactionWriteRepository writeRepository,
            ITransactionReadRepository backupReadRepository,
            ILogger logger) : base(readRepository, writeRepository, backupReadRepository, logger)
        {
        }
    }
}
