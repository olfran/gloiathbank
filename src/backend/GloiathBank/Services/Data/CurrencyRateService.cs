﻿using Domain;
using Domain.Logging;
using Domain.Repositories.Read;
using Domain.Repositories.Write;

namespace Services.Data
{
    /// <summary>
    /// Retrieves all the Currency Rates information from the data source
    /// </summary>
    public class CurrencyRateService : DataServiceBase<CurrencyRate>, ICurrencyRateService
    {
        public CurrencyRateService(
            ICurrencyRateReadRepository readRepository,
            ICurrencyRateWriteRepository writeRepository,
            ICurrencyRateReadRepository backupReadRepository,
            ILogger logger) : base(readRepository, writeRepository, backupReadRepository, logger)
        {
        }
    }
}
