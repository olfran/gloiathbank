﻿using Domain;

namespace Services.Data
{
    /// <summary>
    /// Retrieves all the Currency Rates information from the data source
    /// </summary>
    public interface ICurrencyRateService : IDataServiceBase<CurrencyRate>
    {
    }
}
