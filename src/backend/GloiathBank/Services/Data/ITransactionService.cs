﻿using Domain;

namespace Services.Data
{
    /// <summary>
    /// Retrieves all the Transactions information from the data source
    /// </summary>
    public interface ITransactionService : IDataServiceBase<CurrencyTransaction>
    {
    }
}
