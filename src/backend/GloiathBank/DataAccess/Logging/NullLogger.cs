﻿using Domain.Logging;

namespace DataAccess.Logging
{
    /// <summary>
    /// Null Logger for testing purposes
    /// </summary>
    /// <typeparam name="T">Log entity</typeparam>
    public class NullLogger : ILogger
    {
        public void LogError(string message)
        {
            // Method intentionally left empty.
        }

        public void LogInformation(string message)
        {
            // Method intentionally left empty.
        }

        public void LogWarning(string message)
        {
            // Method intentionally left empty.
        }
    }
}
