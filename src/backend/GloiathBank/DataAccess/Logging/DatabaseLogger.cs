﻿using DataAccess.Source.ORM;
using Domain.Logging;

namespace DataAccess.Logging
{
    /// <summary>
    /// Null Logger for testing purposes
    /// </summary>
    public class DatabaseLogger : ILogger
    {
        private readonly GloiathContext _context;

        public DatabaseLogger(GloiathContext context)
        {
            _context = context;
        }

        public void LogInformation(string message)
        {
            _context.LogInfoes.Add(new LogInfo
            {
                Level = LogLevel.Information,
                Message = message
            });
            _context.SaveChanges();
        }

        public void LogWarning(string message)
        {
            _context.LogInfoes.Add(new LogInfo
            {
                Level = LogLevel.Warning,
                Message = message
            });
            _context.SaveChanges();
        }

        public void LogError(string message)
        {
            _context.LogInfoes.Add(new LogInfo
            {
                Level = LogLevel.Error,
                Message = message
            });
            _context.SaveChanges();
        }
    }
}
