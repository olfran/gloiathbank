﻿using System;
using System.Collections.Generic;
using Domain;

namespace DataAccess.Source.Write
{
    /// <summary>
    /// Save data in the data store
    /// </summary>
    /// <typeparam name="T">DomainBase</typeparam>
    public interface IWriteContext<T> where T : DomainBase
    {
        /// <summary>
        /// Save all the data
        /// </summary>
        /// <param name="entities">Entities to save</param>
        void SaveAll(IEnumerable<T> entities);

        /// <summary>
        /// Remove all the data from the Entity's type
        /// </summary>
        /// <param name="">Entity Type to remove</param>
        void RemoveAll(Type entityType);
    }
}