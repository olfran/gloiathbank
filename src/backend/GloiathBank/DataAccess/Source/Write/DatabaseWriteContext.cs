﻿using DataAccess.Source.ORM;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace DataAccess.Source.Write
{
    /// <summary>
    /// Saves cache in SQLite
    /// </summary>
    /// <typeparam name="T">DomainBase</typeparam>
    public class DatabaseWriteContext<T> : IWriteContext<T> where T : DomainBase
    {
        private readonly GloiathContext _context;

        public DatabaseWriteContext(GloiathContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Save all the data
        /// </summary>
        /// <param name="entities">Entities to save</param>
        public void SaveAll(IEnumerable<T> entities)
        {
            _context.Set<T>().AddRange(entities);
            _context.SaveChanges();
        }

        /// <summary>
        /// Remove all the data from the Entity's type
        /// </summary>
        /// <param name="entityType">Entity Type</param>
        public void RemoveAll(Type entityType)
        {
            _context.Database.ExecuteSqlCommand("DELETE FROM " + entityType.Name);
        }
    }
}
