﻿using Domain;
using System;
using System.Collections.Generic;

namespace DataAccess.Source.Write
{
    /// <summary>
    /// Null Object Pattern, for testing purposes
    /// </summary>
    /// <typeparam name="T">DomainBase</typeparam>
    public class NullWriteContext<T> : IWriteContext<T> where T : DomainBase
    {
        // Dummy method
        public void SaveAll(IEnumerable<T> entities)
        {
            // Method intentionally left empty.
        }

        // Dummy method
        public void RemoveAll(Type entityType)
        {
            // Method intentionally left empty.
        }
    }
}
