﻿using DataAccess.Source.ORM;
using Domain;
using System.Linq;

namespace DataAccess.Source.Read
{
    /// <summary>
    /// Read data from the Database
    /// </summary>
    /// <typeparam name="T">DomainBase</typeparam>
    public class DatabaseReadContext<T> : IReadContext<T> where T : DomainBase
    {
        private readonly GloiathContext _context;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="url">Webservice's URL (it must return a JSON content)</param>
        public DatabaseReadContext(GloiathContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Retrieves all the data from the origin
        /// </summary>
        /// <returns>List of T</returns>
        public IQueryable<T> GetAll()
        {
            return _context.Set<T>();
        }
    }
}
