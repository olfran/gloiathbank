﻿using Domain;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace DataAccess.Source.Read
{
    /// <summary>
    /// Gather JSON data from the WebService
    /// </summary>
    /// <typeparam name="T">DomainBase</typeparam>
    public class WebServiceReadContext<T> : IReadContext<T> where T : DomainBase
    {
        private readonly string _url;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="url">Webservice's URL (it must return a JSON content)</param>
        public WebServiceReadContext(string url)
        {
            _url = url;
        }

        /// <summary>
        /// Retrieves all the data from the origin
        /// </summary>
        /// <returns>List of T</returns>
        public IQueryable<T> GetAll()
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage get = client.GetAsync(_url).Result;
            string content = get.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<List<T>>(content).AsQueryable();
        }
    }
}
