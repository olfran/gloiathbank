﻿using System.Linq;
using Domain;

namespace DataAccess.Source.Read
{
    /// <summary>
    /// Retrieves JSON data from the WebService
    /// </summary>
    /// <typeparam name="T">DomainBase</typeparam>
    public interface IReadContext<T> where T : DomainBase
    {
        /// <summary>
        /// Retrieves all the data from the origin
        /// </summary>
        /// <returns>List of T</returns>
        IQueryable<T> GetAll();
    }
}