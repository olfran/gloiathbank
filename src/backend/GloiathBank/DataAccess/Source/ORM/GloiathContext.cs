﻿using Domain;
using Domain.Logging;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Source.ORM
{
    /// <summary>
    /// Entity Framework Database Context
    /// </summary>
    public class GloiathContext : DbContext
    {
        private readonly string _connectionString;

        public DbSet<CurrencyTransaction> Transactions { get; set; }
        public DbSet<CurrencyRate> CurrencyRates { get; set; }
        public DbSet<LogInfo> LogInfoes { get; set; }

        public GloiathContext(string connectionString)
        {
            _connectionString = connectionString;
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CurrencyTransaction>().HasIndex(x => x.Id);
            modelBuilder.Entity<CurrencyTransaction>()
                .ToTable(nameof(CurrencyTransaction))
                .HasData(new CurrencyTransaction { Id = 1, SKU = "Default", Currency = "USD", Amount = 1M });
            modelBuilder.Entity<CurrencyRate>().HasIndex(x => x.Id);
            modelBuilder.Entity<CurrencyRate>()
                .ToTable(nameof(CurrencyRate))
                .HasData(new CurrencyRate { Id = 1, From = "USD", To = "EUR", Rate = 1M });
            modelBuilder.Entity<LogInfo>().HasIndex(x => x.Id);
            modelBuilder.Entity<LogInfo>().ToTable(nameof(LogInfo));
            base.OnModelCreating(modelBuilder);
        }
    }
}
