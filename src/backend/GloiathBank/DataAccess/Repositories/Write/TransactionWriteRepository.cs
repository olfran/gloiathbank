﻿using DataAccess.Source.Write;
using Domain;
using Domain.Repositories.Write;

namespace DataAccess.Repositories.Read
{
    /// <summary>
    /// Repository for Transactions
    /// </summary>
    public class TransactionWriteRepository : BaseWriteRepository<CurrencyTransaction>, ITransactionWriteRepository
    {
        public TransactionWriteRepository(IWriteContext<CurrencyTransaction> context) : base(context)
        {
        }
    }
}
