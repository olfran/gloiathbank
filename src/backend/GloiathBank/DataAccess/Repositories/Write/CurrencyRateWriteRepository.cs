﻿using DataAccess.Source.Write;
using Domain;
using Domain.Repositories.Write;

namespace DataAccess.Repositories.Read
{
    /// <summary>
    /// Repository for Currency Rates
    /// </summary>
    public class CurrencyRateWriteRepository : BaseWriteRepository<CurrencyRate>, ICurrencyRateWriteRepository
    {
        public CurrencyRateWriteRepository(IWriteContext<CurrencyRate> context) : base(context)
        {
        }
    }
}
