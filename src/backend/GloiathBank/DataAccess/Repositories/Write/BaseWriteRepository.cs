﻿using DataAccess.Source.Write;
using Domain;
using Domain.Repositories.Write;
using System;
using System.Collections.Generic;

namespace DataAccess.Repositories.Read
{
    /// <summary>
    /// Base class for the writable repositories
    /// We define a base class to reuse the common code among the repositores
    /// </summary>
    public class BaseWriteRepository<T> : IWriteRepository<T> where T : DomainBase
    {
        private readonly IWriteContext<T> _context;

        /// <summary>
        /// In this case we need an abstraction, I've called it a "context". The implementation
        /// could retrieve data from either a Web Service or a Database, or any data source we are
        /// applying the Dependency Inversion principle 
        /// </summary>
        /// <param name="context"></param>
        public BaseWriteRepository(IWriteContext<T> context)
        {
            _context = context;
        }

        public void SaveAll(IEnumerable<T> entities)
        {
            _context.SaveAll(entities);
        }

        public void RemoveAll(Type entityType)
        {
            _context.RemoveAll(entityType);
        }
    }
}
