﻿using DataAccess.Source.Read;
using Domain;
using Domain.Repositories.Read;
using System.Linq;

namespace DataAccess.Repositories.Read
{
    /// <summary>
    /// Base class for the readable repositories
    /// We define a base class to reuse the common code among the repositores
    /// </summary>
    /// <typeparam name="T">DomainBase</typeparam>
    public class BaseReadRepository<T> : IReadRepository<T> where T : DomainBase
    {
        private readonly IReadContext<T> _context;

        /// <summary>
        /// In this case we need an abstraction, I've called it a "context". The implementation
        /// could retrieve data from either a Web Service or a Database, or any data source we are
        /// applying the Dependency Inversion principle 
        /// </summary>
        /// <param name="context"></param>
        public BaseReadRepository(IReadContext<T> context)
        {
            _context = context;
        }

        public IQueryable<T> GetAll()
        {
            return _context.GetAll();
        }
    }
}
