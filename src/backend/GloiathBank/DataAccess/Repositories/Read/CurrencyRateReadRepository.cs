﻿using DataAccess.Source.Read;
using Domain;
using Domain.Repositories.Read;

namespace DataAccess.Repositories.Read
{
    /// <summary>
    /// Repository for Currency Rates
    /// </summary>
    public class CurrencyRateReadRepository : BaseReadRepository<CurrencyRate>, ICurrencyRateReadRepository
    {
        public CurrencyRateReadRepository(IReadContext<CurrencyRate> context) : base(context)
        {
        }
    }
}
