﻿using DataAccess.Source.Read;
using Domain;
using Domain.Repositories.Read;

namespace DataAccess.Repositories.Read
{
    /// <summary>
    /// Repository for Transactions
    /// </summary>
    public class TransactionReadRepository : BaseReadRepository<CurrencyTransaction>, ITransactionReadRepository
    {
        public TransactionReadRepository(IReadContext<CurrencyTransaction> context) : base(context)
        {
        }
    }
}
