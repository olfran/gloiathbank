﻿namespace Domain
{
    /// <summary>
    /// Represents a Currency Rate
    /// </summary>
    /// <remarks>
    /// Decimal type is recommended for monetary calculations
    /// See https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/decimal
    /// </remarks>
    public class CurrencyRate : DomainBase
    {
        public long Id { get; set; }
        /// <summary>
        /// Currency code. I.E: AUD, EUR, USD
        /// </summary>
        public string From { get; set; }
        public string To { get; set; }
        public decimal Rate { get; set; }
    }
}
