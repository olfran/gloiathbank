﻿using System;

namespace Domain.Exceptions
{
    public class NoDataAvailableException : Exception
    {
        public NoDataAvailableException() : base("No data available neither from the default data source nor the backup, we're doomed")
        {
        }

        public NoDataAvailableException(string message) : base(message)
        {
        }

        public NoDataAvailableException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoDataAvailableException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
        {
        }
    }
}
