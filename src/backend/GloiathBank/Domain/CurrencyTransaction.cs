﻿namespace Domain
{
    /// <summary>
    /// Represents a Transaction
    /// </summary>
    /// <remarks>
    /// Decimal type is recommended for monetary calculations
    /// See https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/decimal
    /// </remarks>
    public class CurrencyTransaction : DomainBase
    {
        public long Id { get; set; }
        public string SKU { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
    }
}
