﻿using System;

namespace Domain.Logging
{
    /// <summary>
    /// Represents a logging information
    /// </summary>
    public class LogInfo : DomainBase
    {
        public long Id { get; set; }
        public LogLevel Level { get; set; }
        public string Message { get; set; }
        public DateTime Timestamp { get; set; } = DateTime.Now;
    }
}
