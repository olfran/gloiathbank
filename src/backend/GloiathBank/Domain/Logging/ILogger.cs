﻿namespace Domain.Logging
{
    /// <summary>
    /// Very simple Log interface to Log events
    /// </summary>
    public interface ILogger
    {
        void LogInformation(string message);
        void LogWarning(string message);
        void LogError(string message);
    }
}
