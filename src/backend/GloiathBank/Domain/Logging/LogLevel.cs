﻿namespace Domain.Logging
{
    public enum LogLevel
    {
        Information,
        Warning,
        Error
    }
}
