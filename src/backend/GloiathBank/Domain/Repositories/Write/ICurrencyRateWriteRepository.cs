﻿namespace Domain.Repositories.Write
{
    /// <summary>
    /// Repository for Currency Rates
    /// </summary>
    public interface ICurrencyRateWriteRepository : IWriteRepository<CurrencyRate>
    {
    }
}
