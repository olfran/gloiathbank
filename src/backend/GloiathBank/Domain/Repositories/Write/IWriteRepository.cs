﻿using System;
using System.Collections.Generic;

namespace Domain.Repositories.Write
{
    /// <summary>
    /// Base interface for all 'Write Only' repositores
    /// </summary>
    /// <typeparam name="T">DomainBase</typeparam>
    public interface IWriteRepository<T> where T : DomainBase
    {
        void SaveAll(IEnumerable<T> entities);
        void RemoveAll(Type entityType);
    }
}
