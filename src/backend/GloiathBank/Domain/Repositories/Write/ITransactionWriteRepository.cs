﻿namespace Domain.Repositories.Write
{
    /// <summary>
    /// Repository for Transactions
    /// </summary>
    public interface ITransactionWriteRepository : IWriteRepository<CurrencyTransaction>
    {
    }
}
