﻿namespace Domain.Repositories.Read
{
    /// <summary>
    /// Repository for Transactions
    /// </summary>
    public interface ITransactionReadRepository : IReadRepository<CurrencyTransaction>
    {
    }
}
