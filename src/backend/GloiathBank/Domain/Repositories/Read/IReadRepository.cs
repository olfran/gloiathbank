﻿using System.Linq;

namespace Domain.Repositories.Read
{
    /// <summary>
    /// Base interface for all 'Read Only' repositores
    /// </summary>
    /// <typeparam name="T">DomainBase</typeparam>
    public interface IReadRepository<T> where T : DomainBase
    {
        IQueryable<T> GetAll();
    }
}
