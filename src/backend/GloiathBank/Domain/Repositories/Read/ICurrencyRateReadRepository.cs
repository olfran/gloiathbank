﻿namespace Domain.Repositories.Read
{
    /// <summary>
    /// Repository for Currency Rates
    /// </summary>
    public interface ICurrencyRateReadRepository : IReadRepository<CurrencyRate>
    {
    }
}
