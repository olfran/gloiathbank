﻿using Domain;
using Domain.Exceptions;
using Domain.Logging;
using Domain.Repositories.Read;
using Domain.Repositories.Write;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Services.Data;
using System.Collections.Generic;
using System.Linq;

namespace Services.Tests.Data
{
    [TestClass]
    public class CurrencyRateServiceTests
    {
        // SUT
        private ICurrencyRateService _currencyRateService;
        private Mock<ICurrencyRateReadRepository> _mockReadRepository;
        private Mock<ICurrencyRateWriteRepository> _mockWriteRepository;
        private Mock<ICurrencyRateReadRepository> _mockBackupReadRepository;
        private Mock<ILogger> _mockLogger;

        private readonly List<CurrencyRate> _defaultCurrencyRate = new List<CurrencyRate>
        {
            new CurrencyRate { From = "USD", To = "CAD", Rate = 1.22M }
        };

        private readonly List<CurrencyRate> _backupCurrencyRate = new List<CurrencyRate>
        {
            new CurrencyRate { From = "USD", To = "CAD", Rate = 1.02M }
        };

        [TestInitialize]
        public void SetUp()
        {
            _mockReadRepository = new Mock<ICurrencyRateReadRepository>();
            _mockWriteRepository = new Mock<ICurrencyRateWriteRepository>();
            _mockBackupReadRepository = new Mock<ICurrencyRateReadRepository>();
            _mockLogger = new Mock<ILogger>();
            _mockReadRepository.Setup(m => m.GetAll()).Returns(() => _defaultCurrencyRate.AsQueryable());
            _mockWriteRepository.Setup(m => m.SaveAll(It.IsAny<IEnumerable<CurrencyRate>>()));
            _mockBackupReadRepository.Setup(m => m.GetAll()).Returns(() => _backupCurrencyRate.AsQueryable());
            _mockLogger.Setup(m => m.LogInformation(It.IsAny<string>()));
            _mockLogger.Setup(m => m.LogWarning(It.IsAny<string>()));
            _mockLogger.Setup(m => m.LogError(It.IsAny<string>()));
        }

        [TestMethod]
        public void GetAllShouldWork()
        {
            // Arrange
            _currencyRateService = new CurrencyRateService(_mockReadRepository.Object, _mockWriteRepository.Object, _mockBackupReadRepository.Object, _mockLogger.Object);
            // Act
            var result = _currencyRateService.GetAll(null, out bool fromBackup);
            // Assert
            Assert.AreEqual(_defaultCurrencyRate[0].Rate, result.First().Rate);
            Assert.AreEqual(false, fromBackup);
            // Should Log Information
            _mockLogger.Verify(m => m.LogInformation(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [TestMethod]
        public void GetAllShouldReturnDataFromBackup()
        {
            // Arrange
            _mockReadRepository.Setup(m => m.GetAll()).Returns(() => null);
            _currencyRateService = new CurrencyRateService(_mockReadRepository.Object, _mockWriteRepository.Object, _mockBackupReadRepository.Object, _mockLogger.Object);
            // Act
            var result = _currencyRateService.GetAll(null, out bool fromBackup);
            // Assert
            Assert.AreEqual(_backupCurrencyRate[0].Rate, result.First().Rate);
            Assert.AreEqual(true, fromBackup);
            // Should Log Warning
            _mockLogger.Verify(m => m.LogWarning(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void GetAllShouldThrowNoDataAvailableException()
        {
            // Arrange
            _mockReadRepository.Setup(m => m.GetAll()).Returns(() => null);
            _mockBackupReadRepository.Setup(m => m.GetAll()).Returns(() => null);
            _currencyRateService = new CurrencyRateService(_mockReadRepository.Object, _mockWriteRepository.Object, _mockBackupReadRepository.Object, _mockLogger.Object);
            // Act
            object result() => _currencyRateService.GetAll(null, out bool fromBackup);
            // Assert
            Assert.ThrowsException<NoDataAvailableException>(result);
            //Should Log Error
            _mockLogger.Verify(m => m.LogError(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void GetAllShouldSaveDataInBackup()
        {
            // Arrange
            _currencyRateService = new CurrencyRateService(_mockReadRepository.Object, _mockWriteRepository.Object, _mockBackupReadRepository.Object, _mockLogger.Object);
            // Act
            _currencyRateService.GetAll(null, out bool fromBackup);
            // Assert
            _mockWriteRepository.Verify(m => m.SaveAll(It.IsAny<IEnumerable<CurrencyRate>>()), Times.Once);
        }

        [TestMethod]
        public void GetAllShouldFilterDataWithWhere()
        {
            // Arrange
            _currencyRateService = new CurrencyRateService(_mockReadRepository.Object, _mockWriteRepository.Object, _mockBackupReadRepository.Object, _mockLogger.Object);
            // Act
            var result = _currencyRateService.GetAll(model => model.From == "USD", out bool fromBackup);
            // Assert
            Assert.AreEqual(_defaultCurrencyRate[0].Rate, result.First().Rate);
        }
    }
}
