﻿using Domain;
using Domain.Exceptions;
using Domain.Logging;
using Domain.Repositories.Read;
using Domain.Repositories.Write;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services.Tests.Data
{
    [TestClass]
    public class TransactionServiceTest
    {
        // SUT
        private ITransactionService _transactionService;
        private Mock<ITransactionReadRepository> _mockReadRepository;
        private Mock<ITransactionWriteRepository> _mockWriteRepository;
        private Mock<ITransactionReadRepository> _mockBackupReadRepository;
        private Mock<ILogger> _mockLogger;

        private readonly List<CurrencyTransaction> _defaultTransaction = new List<CurrencyTransaction>
        {
            new CurrencyTransaction { Amount = 1, Currency = "USD", SKU = "A" }
        };

        private readonly List<CurrencyTransaction> _backupTransaction = new List<CurrencyTransaction>
        {
            new CurrencyTransaction { Amount = 2, Currency = "EUR", SKU = "B" }
        };

        [TestInitialize]
        public void SetUp()
        {
            _mockReadRepository = new Mock<ITransactionReadRepository>();
            _mockWriteRepository = new Mock<ITransactionWriteRepository>();
            _mockBackupReadRepository = new Mock<ITransactionReadRepository>();
            _mockLogger = new Mock<ILogger>();
            _mockReadRepository.Setup(m => m.GetAll()).Returns(() => _defaultTransaction.AsQueryable());
            _mockWriteRepository.Setup(m => m.SaveAll(It.IsAny<IEnumerable<CurrencyTransaction>>()));
            _mockBackupReadRepository.Setup(m => m.GetAll()).Returns(() => _backupTransaction.AsQueryable());
            _mockLogger.Setup(m => m.LogInformation(It.IsAny<string>()));
            _mockLogger.Setup(m => m.LogWarning(It.IsAny<string>()));
            _mockLogger.Setup(m => m.LogError(It.IsAny<string>()));
        }

        [TestMethod]
        public void GetAllShouldWork()
        {
            // Arrange
            _transactionService = new TransactionService(_mockReadRepository.Object, _mockWriteRepository.Object, _mockBackupReadRepository.Object, _mockLogger.Object);
            // Act
            var result = _transactionService.GetAll(null, out bool fromBackup);
            // Assert
            Assert.AreEqual(_defaultTransaction[0].SKU, result.First().SKU);
            Assert.AreEqual(false, fromBackup);
            // Should Log Information
            _mockLogger.Verify(m => m.LogInformation(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [TestMethod]
        public void GetAllShouldReturnDataFromBackup()
        {
            // Arrange
            _mockReadRepository.Setup(m => m.GetAll()).Returns(() => null);
            _transactionService = new TransactionService(_mockReadRepository.Object, _mockWriteRepository.Object, _mockBackupReadRepository.Object, _mockLogger.Object);
            // Act
            var result = _transactionService.GetAll(null, out bool fromBackup);
            // Assert
            Assert.AreEqual(_backupTransaction[0].Id, result.First().Id);
            Assert.AreEqual(true, fromBackup);
            // Should Log Warning
            _mockLogger.Verify(m => m.LogWarning(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void GetAllShouldThrowNoDataAvailableException()
        {
            // Arrange
            _mockReadRepository.Setup(m => m.GetAll()).Returns(() => null);
            _mockBackupReadRepository.Setup(m => m.GetAll()).Returns(() => null);
            _transactionService = new TransactionService(_mockReadRepository.Object, _mockWriteRepository.Object, _mockBackupReadRepository.Object, _mockLogger.Object);
            // Act
            object result() => _transactionService.GetAll(null, out bool fromBackup);
            // Assert
            Assert.ThrowsException<NoDataAvailableException>(result);
            //Should Log Error
            _mockLogger.Verify(m => m.LogError(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void GetAllShouldSaveDataInBackup()
        {
            // Arrange
            _transactionService = new TransactionService(_mockReadRepository.Object, _mockWriteRepository.Object, _mockBackupReadRepository.Object, _mockLogger.Object);
            // Act
            _transactionService.GetAll(null, out bool fromBackup);
            // Assert
            _mockWriteRepository.Verify(m => m.SaveAll(It.IsAny<IEnumerable<CurrencyTransaction>>()), Times.Once);
        }

        [TestMethod]
        public void GetAllShouldFilterDataWithWhere()
        {
            // Arrange
            _transactionService = new TransactionService(_mockReadRepository.Object, _mockWriteRepository.Object, _mockBackupReadRepository.Object, _mockLogger.Object);
            // Act
            var result = _transactionService.GetAll(model => model.SKU == "A", out bool fromBackup);
            // Assert
            Assert.AreEqual(_defaultTransaction[0].SKU, result.First().SKU);
        }
    }
}
