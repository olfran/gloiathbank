import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { CurrencyRateService } from '../../services/currencyrate.service';
import { TransactionService } from '../../services/transaction.service';
import { TransactionViewModel, Transaction } from '../../models/transaction.model';
import { RateViewModel } from '../../models/currencyrate.model';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent implements OnInit {
  query: string;
  loading = false;
  totalEuros: 0;
  columnsToDisplay = ['sku', 'currency', 'amount'];
  transactions: Transaction[];

  constructor(
    public currencyRateService: CurrencyRateService,
    public transactionRateService: TransactionService,
    public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.handleError = this.handleError.bind(this);
  }

  search() {
    if (!this.query) {
      this.snackBar.open('The search text cannot be empty', 'Ok', { duration: 6000 });
      return;
    }
    this.loading = true;
    this.transactionRateService.getTransactions(this.query)
    .subscribe((tran: TransactionViewModel) => {
      this.currencyRateService.getRates().subscribe((rate: RateViewModel) => {
        this.checkIsFromBackup(tran, rate);
        const result: Transaction[] = [];
        this.totalEuros = 0;
        for (const t of tran.transactions) {
          const exchange = this.currencyRateService.exchange(rate, t.amount, t.currency, 'EUR');
          this.totalEuros += exchange;
          result.push({ amount: exchange, sku: t.sku,  currency: t.currency });
        }
        this.transactions = result;
        this.loading = false;
      }, this.handleError);
    }, this.handleError);
  }

  checkIsFromBackup(tran: TransactionViewModel, rate: RateViewModel) {
    if (tran.fromBackup) {
      this.snackBar.open('The transaction data is coming from backup, probably there was an error fetching the remote WS', 'Ok', { duration: 6000 });
    }
    if (rate.fromBackup) {
      this.snackBar.open('The rate data is coming from backup', 'Ok', { duration: 6000 });
    }
  }

  handleError(error) {
    this.loading = false;
    this.transactions = null;
    this.snackBar.open('Error retrieving the data from the WS, is the ASP.NET Core backend running?', 'Ok');
  }
}
