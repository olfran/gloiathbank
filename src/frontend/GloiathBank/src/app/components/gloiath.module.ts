import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchBoxComponent } from './search-box/search-box.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule, MatInputModule, MatButtonModule, MatSnackBarModule, MatProgressSpinnerModule, MatTableModule, MatCardModule, MatTabsModule, MatProgressBarModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CurrencyRateService } from '../services/currencyrate.service';
import { HttpClientModule } from '@angular/common/http';
import { TransactionService } from '../services/transaction.service';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatTableModule,
    MatCardModule,
    MatProgressBarModule,
    HttpClientModule,
  ],
  providers: [
    CurrencyRateService,
    TransactionService
  ],
  declarations: [
    SearchBoxComponent
  ],
  exports: [
    SearchBoxComponent,
  ]
})
export class GloiathModule { }
