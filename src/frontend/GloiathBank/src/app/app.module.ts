import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GloiathModule } from './components/gloiath.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    GloiathModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
