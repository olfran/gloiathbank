interface CurrencyRate {
  from: string;
  to: string;
  rate: number;
}

export interface RateViewModel {
  rates: CurrencyRate[];
  fromBackup: boolean;
}
