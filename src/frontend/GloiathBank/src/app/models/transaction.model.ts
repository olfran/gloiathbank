export interface Transaction {
    sku: string;
    amount: number;
    currency: string;
}

export interface TransactionViewModel {
    transactions: Transaction[];
    fromBackup: boolean;
}
