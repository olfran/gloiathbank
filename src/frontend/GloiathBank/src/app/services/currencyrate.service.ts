import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RateViewModel } from '../models/currencyrate.model';
import { Config } from '../app.config';

@Injectable()
export class CurrencyRateService {
  constructor(private http: HttpClient) {
  }

  getRates(): Observable<RateViewModel> {
    return this.http
    .get<RateViewModel>(`${Config.baseUrl}/rates`);
  }

  exchange(data: RateViewModel, amount: number, from: string, to: string): number {
    if (from === to) {
      return amount;
    }
    const fromList = [], toList = [];
    for (const row of data.rates) {
      if (row.from === from) {
        fromList.push(row);
      }
      if (row.to === to) {
        toList.push(row);
      }
      if (row.from === from && row.to === to) {
        return row.rate * amount;
      }
    }
    // We couldn't find any suitable for the exchange data let's try to find it
    // Check if There is any way to make this exchange with the current information
    // Este algoritmo es O(n*m) puede ser O(n^2) en algunos casos, pero como son pocos datos
    // igual no afecta notablemente el rendimiento
    if (fromList && toList) {
      for (const rowFrom of fromList) {
        const foundTo =  toList.find(x => rowFrom.to === x.from);
        if (foundTo) {
          return rowFrom.rate * amount * foundTo.rate;
        }
      }
    }
    // No luck let's get out of here
    return -1;
  }
}
