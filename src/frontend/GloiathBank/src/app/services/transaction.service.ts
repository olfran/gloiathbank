import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RateViewModel } from '../models/currencyrate.model';
import { Config } from '../app.config';
import { TransactionViewModel } from '../models/transaction.model';

@Injectable()
export class TransactionService {
  constructor(private http: HttpClient) {
  }

  getTransactions(query: string): Observable<TransactionViewModel> {
    return this.http
    .get<TransactionViewModel>(`${Config.baseUrl}/transactions?sku=${query}`);
  }
}
